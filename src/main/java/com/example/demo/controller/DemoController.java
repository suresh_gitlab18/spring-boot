package com.example.demo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {
	@RequestMapping (value = "/demoController")
	public String test() {
		return "Spring Demo Success";
	}
	@RequestMapping (value = "/testController2")
	public String test2() {
		return "Spring Test Success";
	}
}
